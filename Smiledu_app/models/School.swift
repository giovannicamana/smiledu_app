//
//  School.swift
//  Smiledu_app
//
//  Created by Giovanni Camana on 2/23/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import Foundation
import UIKit
class School {
    var name : String = ""
    var sede : String = ""
    var db:String = "smiledu"
//    var icon: String = ""
    var icon: UIImage
    var order: String = "asc"

    init(name:String, sede:String, db:String, icon:String, order: String) {
        self.name = name
        self.sede = sede
        self.db = db
        self.order = order
        if let img = UIImage(named: icon) {
            self.icon = img
        } else {
            self.icon = UIImage(named: "default")!
        }
//        self.icon = UIImage(named: icon)
    }
}
