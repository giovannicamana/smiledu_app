//
//  SchoolLine.swift
//  Smiledu_app
//
//  Created by Giovanni Camana on 2/23/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import Foundation
import UIKit

class SchoolLine {
    var schools : [School]
    var name: String
    init(name:String , includeSchools: [School]) {
        self.schools = includeSchools
        self.name = name
    }
    
    class func schoolLines() -> [SchoolLine] {
        return [self._schools()]
    }
    
    private class func _schools() -> SchoolLine {
        var schools = [School]()

//        schools.append(School(name: "Avantgard", sede: "Lima - Perú", db: "smile", icon: "https://source.unsplash.com/64x64", order: "asc"))
//        schools.append(School(name: "La Ascencion", sede: "Lima - Perú", db: "smiledu", icon: "https://source.unsplash.com/64x64", order: "asc"))
//        schools.append(School(name: "Luz y vida", sede: "Lima - Perú", db: "smiledu", icon: "https://source.unsplash.com/64x64", order: "asc"))
        schools.append(School(name: "Avantgard", sede: "Lima - Perú", db: "smile", icon: "icon_avantgard", order: "asc"))
        schools.append(School(name: "Country", sede: "Lima - Perú", db: "smiledu", icon: "icon_country", order: "asc"))
        schools.append(School(name: "Columbus", sede: "Lima - Perú", db: "smiledu", icon: "icon_columbus", order: "asc"))
        schools.append(School(name: "Nosce", sede: "Lima - Perú", db: "smiledu", icon: "icon_nosce", order: "asc"))
        schools.append(School(name: "Sumer", sede: "Lima - Perú", db: "smiledu", icon: "icon_sumer", order: "asc"))
        schools.append(School(name: "Avantgard", sede: "Lima - Perú", db: "smile", icon: "searchHeader", order: "asc"))
        schools.append(School(name: "Country", sede: "Lima - Perú", db: "smiledu", icon: "searchHeader", order: "asc"))
        schools.append(School(name: "Columbus", sede: "Lima - Perú", db: "smiledu", icon: "searchHeader", order: "asc"))
        schools.append(School(name: "Nosce", sede: "Lima - Perú", db: "smiledu", icon: "searchHeader", order: "asc"))
        schools.append(School(name: "Sumer", sede: "Lima - Perú", db: "smiledu", icon: "searchHeader", order: "asc"))
        schools.append(School(name: "Avantgard", sede: "Lima - Perú", db: "smile", icon: "icon_avantgard", order: "asc"))
        schools.append(School(name: "Country", sede: "Lima - Perú", db: "smiledu", icon: "icon_country", order: "asc"))
        schools.append(School(name: "Columbus", sede: "Lima - Perú", db: "smiledu", icon: "icon_columbus", order: "asc"))
        schools.append(School(name: "Nosce", sede: "Lima - Perú", db: "smiledu", icon: "icon_nosce", order: "asc"))
        schools.append(School(name: "Sumer", sede: "Lima - Perú", db: "smiledu", icon: "icon_sumer", order: "asc"))
        schools.append(School(name: "Avantgard", sede: "Lima - Perú", db: "smile", icon: "icon_avantgard", order: "asc"))
        schools.append(School(name: "Country", sede: "Lima - Perú", db: "smiledu", icon: "icon_country", order: "asc"))
        schools.append(School(name: "Columbus", sede: "Lima - Perú", db: "smiledu", icon: "icon_columbus", order: "asc"))
        schools.append(School(name: "Nosce", sede: "Lima - Perú", db: "smiledu", icon: "icon_nosce", order: "asc"))
        schools.append(School(name: "Sumer", sede: "Lima - Perú", db: "smiledu", icon: "icon_sumer", order: "asc"))
        schools.append(School(name: "Avantgard", sede: "Lima - Perú", db: "smile", icon: "icon_avantgard", order: "asc"))
        schools.append(School(name: "Country", sede: "Lima - Perú", db: "smiledu", icon: "icon_country", order: "asc"))
        schools.append(School(name: "Columbus", sede: "Lima - Perú", db: "smiledu", icon: "icon_columbus", order: "asc"))
        schools.append(School(name: "Nosce", sede: "Lima - Perú", db: "smiledu", icon: "icon_nosce", order: "asc"))
        schools.append(School(name: "Sumer", sede: "Lima - Perú", db: "smiledu", icon: "searchHeader", order: "asc"))
        return SchoolLine(name: "Más descargados", includeSchools: schools)
    }
    
}
