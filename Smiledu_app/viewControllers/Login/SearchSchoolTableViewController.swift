//
//  SearchSchoolTableViewController.swift
//  Smiledu_app
//
//  Created by Giovanni Camana on 2/21/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import UIKit
import SDWebImage
import Hex

class SearchSchoolTableViewController: UITableViewController {
    
    let list :Array = ["Avantgard","smiledu", "La Ascencion"]
    
//    var schools: [School] {
//        var schoolLines = SchoolLine.schoolLines()
//        return schoolLines[0].schools
//    }

    lazy var schoolsLines: [SchoolLine] = {
        return SchoolLine.schoolLines()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItems()
    }
    
    func setupNavigationBarItems() {
        navigationController?.navigationBar.prefersLargeTitles = true
        let SearchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = SearchController
        navigationItem.hidesSearchBarWhenScrolling = false

        let titleImageView =  UIImageView(image:#imageLiteral(resourceName: "searchTitle"))
        navigationItem.titleView = titleImageView
    }

    @IBAction func searchBackPage(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return schoolsLines.count
    }
    
//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let schoolLine = schoolsLines[section]
//        return schoolLine.name
//    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?  {
        let headerFrame = tableView.frame
        let schoolLine = schoolsLines[section]

        let label : UILabel = UILabel()

        
        if(section == 0){
            let headerFrame = tableView.frame
            label.text = schoolLine.name
            label.textColor = UIColor(hex: "FF5722")
            label.font = UIFont.boldSystemFont(ofSize: 18)
            label.backgroundColor = UIColor.white
            label.frame = CGRect(x: 16, y: 0, width: headerFrame.size.width - 16, height: 50)

        } else {
            label.textColor = UIColor.white
            label.text = ""
        }

        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: headerFrame.size.width, height: headerFrame.size.height))
        headerView.addSubview(label)

        return headerView
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var headerHeight: CGFloat
        if section == 0 {
            headerHeight = 50
        } else {
            headerHeight = CGFloat.leastNonzeroMagnitude
        }
        return headerHeight
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let schoolLine = schoolsLines[section]
        
        return schoolLine.schools.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCollegeIdentifier", for: indexPath)
        
        
        let schoolLine = schoolsLines[indexPath.section]
        let school = schoolLine.schools[indexPath.row]
        cell.textLabel?.text = school.name
        cell.detailTextLabel?.text = school.sede
        cell.imageView?.image = school.icon
        //cell.imageView?.sd_setImage(with: URL(string: String(describing: schools[indexPath.row].icon)), placeholderImage: UIImage(named: "searchHeader"))

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
