//
//  LoginViewController.swift
//  Smiledu_app
//
//  Created by Anthony Montes Larios on 14/02/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import UIKit
import Hex

class LoginViewController: UIViewController , UITextFieldDelegate {

    
//    @IBOutlet weak var btnIngresar: UIButton!
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true;
        
//         Set an Array Containing Any Number of CGColors
        let linearGradient:[CGColor] = [UIColor(hex: "BF3306").cgColor, UIColor(hex: "FF5722").cgColor]

        view.linearGradientBackground(angleInDegs: 0, colors: linearGradient)
        
        
        self.searchTextField.layer.borderColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha:1).cgColor;
        self.searchTextField.layer.borderWidth = CGFloat(Float(1.0));
        self.searchTextField.layer.cornerRadius = CGFloat(Float(self.searchTextField.frame.size.height / 2))
        self.searchTextField.clipsToBounds = true
        
        self.userTextField.layer.borderColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha:1).cgColor;
        self.userTextField.layer.borderWidth = CGFloat(Float(1.0));
        self.userTextField.layer.cornerRadius = CGFloat(Float(self.searchTextField.frame.size.height / 2))
        self.userTextField.clipsToBounds = true
        
        self.passwordTextField.layer.borderColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha:1).cgColor;
        self.passwordTextField.layer.borderWidth = CGFloat(Float(1.0));
        self.passwordTextField.layer.cornerRadius = CGFloat(Float(self.searchTextField.frame.size.height / 2))
        self.passwordTextField.clipsToBounds = true
        
        
        
        let searchImage = UIImage(named: "searchLoginInput")
        addLeftImageTo(textField: searchTextField, andImage: searchImage!)
        textIndent(textField: passwordTextField)
        textIndent(textField: userTextField)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func checkBoxTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) {(success) in
            sender.isSelected = !sender.isSelected
            print(sender.isSelected)
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                sender.transform = .identity
                
            }, completion: nil)
            
        }
    }
    func addLeftImageTo(textField: UITextField, andImage img: UIImage) {
        let leftImageView  = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 30, height: 30))
        leftImageView.image = img
        textField.leftView = leftImageView
        textField.leftViewMode = .always
    }
    
    func textIndent(textField:UITextField) {
        let leftIndentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        textField.leftViewMode = UITextFieldViewMode.always
        textField.leftView = leftIndentView
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Int {
    func degreesToRads() -> Double {
        return (Double(self) * .pi / 180)
    }
}

extension CGPoint {
    func opposite() -> CGPoint {
        // Create New Point
        var oppositePoint = CGPoint()
        // Get Origin Data
        let originXValue = self.x
        let originYValue = self.y
        // Convert Points and Update New Point
        oppositePoint.x = 1.0 - originXValue
        oppositePoint.y = 1.0 - originYValue
        return oppositePoint
    }
}

extension UIView {
    func startAndEndPointsFrom(angle: Int) -> (startPoint:CGPoint, endPoint:CGPoint) {
        // Set default points for angle of 0°
        var startPointX:CGFloat = 0.5
        var startPointY:CGFloat = 1.0
        
        // Define point objects
        var startPoint:CGPoint
        var endPoint:CGPoint
        
        // Define points
        switch true {
        // Define known points
        case angle == 0:
            startPointX = 0.5
            startPointY = 1.0
        case angle == 45:
            startPointX = 0.0
            startPointY = 1.0
        case angle == 90:
            startPointX = 0.0
            startPointY = 0.5
        case angle == 135:
            startPointX = 0.0
            startPointY = 0.0
        case angle == 180:
            startPointX = 0.5
            startPointY = 0.0
        case angle == 225:
            startPointX = 1.0
            startPointY = 0.0
        case angle == 270:
            startPointX = 1.0
            startPointY = 0.5
        case angle == 315:
            startPointX = 1.0
            startPointY = 1.0
        // Define calculated points
        case angle > 315 || angle < 45:
            startPointX = 0.5 - CGFloat(tan(angle.degreesToRads()) * 0.5)
            startPointY = 1.0
        case angle > 45 && angle < 135:
            startPointX = 0.0
            startPointY = 0.5 + CGFloat(tan(90.degreesToRads() - angle.degreesToRads()) * 0.5)
        case angle > 135 && angle < 225:
            startPointX = 0.5 - CGFloat(tan(180.degreesToRads() - angle.degreesToRads()) * 0.5)
            startPointY = 0.0
        case angle > 225 && angle < 359:
            startPointX = 1.0
            startPointY = 0.5 - CGFloat(tan(270.degreesToRads() - angle.degreesToRads()) * 0.5)
        default: break
        }
        // Build return CGPoints
        startPoint = CGPoint(x: startPointX, y: startPointY)
        endPoint = startPoint.opposite()
        // Return CGPoints
        return (startPoint, endPoint)
    }
    
    func linearGradientBackground(angleInDegs: Int, colors: [CGColor]) {
        // Create New Gradient Layer
        let gradientBaseLayer: CAGradientLayer = CAGradientLayer()
        // Feed in Our Parameters
        gradientBaseLayer.frame = self.frame
        gradientBaseLayer.colors = colors
        gradientBaseLayer.startPoint = startAndEndPointsFrom(angle: angleInDegs).startPoint
        gradientBaseLayer.endPoint = startAndEndPointsFrom(angle: angleInDegs).endPoint
        // Add Our Gradient Layer to the Background
        self.layer.insertSublayer(gradientBaseLayer, at: 0)
    }
}
