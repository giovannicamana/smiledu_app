//
//  RoundButton.swift
//  Smiledu_app
//
//  Created by Giovanni Camana on 2/22/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import UIKit
@IBDesignable
class RoundButton: UIButton {
    @IBInspectable var cornerRadius : CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor  = borderColor.cgColor
        }
    }
    
    @IBInspectable var shadowColor : UIColor = UIColor.clear {
        didSet {
            self.layer.shadowColor  = shadowColor.cgColor
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0 {
        didSet {
            self.layer.shadowRadius  = shadowRadius
        }
    }
    
    @IBInspectable var shadowOffset : CGSize  = CGSize.zero {
        didSet {
            self.layer.shadowOffset  = shadowOffset
        }
    }
    
    //    @IBInspectable var shadowOpacity : CGSize = 0 {
    //        didSet {
    //            self.layer.shadowOpacity  = shadowOpacity
    //        }
    //    }
}
