//
//  constants.swift
//  Smiledu_app
//
//  Created by Giovanni Camana on 2/17/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import Foundation
import UIKit

struct globalVariables {
    static let newsAPI = "https://store_api.smiledu.pe/service/getNoticias?bd=avantgard&id_pers=765&cod_fam=%27F20164430%27&tipo=1"

    static let END_POINT = "https://store_api.smiledu.pe/service"
}
