//
//  NoticiasViewController.swift
//  Smiledu_app
//
//  Created by Anthony Montes Larios on 14/02/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class NewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var newsDictionary = [String:AnyObject]()
    var newArray = [AnyObject]()
    
    @IBOutlet weak var newsTableView: UITableView!
    override func viewDidLoad() {
        
        Alamofire.request(globalVariables.newsAPI).responseJSON { response in
            if let json = response.result.value {
                self.newsDictionary = json as! [String : AnyObject]
                self.newArray = self.newsDictionary["response"] as! [AnyObject]
                print("Imprimiendo JSON \(self.newArray)")
                print("contando \(self.newArray.count)")
                self.newsTableView.reloadData()
            }
        }
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
//    func setupNavigationBarItems() {
//        navigationController?.navigationBar.prefersLargeTitles = true
//        let SearchController = UISearchController(searchResultsController: nil)
//        navigationItem.searchController = SearchController
//        navigationItem.hidesSearchBarWhenScrolling = false
//
//        let titleImageView =  UIImageView(image:#imageLiteral(resourceName: "searchTitle"))
//        navigationItem.titleView = titleImageView
//    }
    
    func searchBarSetup(){
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 60, width: (UIScreen.main.bounds.width), height: 70))
        searchBar.showsScopeBar = true
        searchBar.scopeButtonTitles = ["Todas","Circulares","Populares","Favoritos"]
        
        searchBar.delegate = self
        self.newsTableView.tableHeaderView = searchBar
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNewsList", for: indexPath) as! newsTableViewCell
        
        //Texto Titulo
        cell.newsListTitle.text = newArray[indexPath.row]["titulo"] as? String
        //Cuerpo
        
        //Fecha --
        let newDate = newArray[indexPath.row]["fecha_publicacion"] as? String
        //Imagen
        let newImagen:[String] = newArray[indexPath.row]["foto_noticia"] as!  [String]
        
        print(type(of:  newArray[indexPath.row]["foto_noticia"]))
        
        print(type(of: newImagen))
        
        cell.newsListImage.sd_setImage(with: URL(string: String(describing: newImagen[0])), placeholderImage: UIImage(named: "smiledu_smiledustore"))
        
        cell.newsListImage.layer.cornerRadius = 13
        cell.newsListImage.clipsToBounds = true
        cell.newsListImage.animationDuration = 10
        
        cell.newsListImage.autoresizesSubviews = true
        cell.newsListImage.startAnimating()
        
        
        
        cell.newsListImageFilter.layer.cornerRadius = 13
        cell.newsListImageFilter.clipsToBounds = true
        cell.newsListImageFilter.animationDuration = 10
        
        
        //cell.newsListImage.layer.borderWidth = 3
        
        
        print("Foto Notica 1 \(newArray[indexPath.row]["foto_noticia"])")
        
        print("Foto Notica 2 \(newImagen)")
        
        
        return cell
    }
    
    
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsScopeBar = true
        searchBar.sizeToFit()
        searchBar.setShowsCancelButton(true, animated: true)
        
        return true
    }
    
    public func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsScopeBar = false
        searchBar.sizeToFit()
        searchBar.setShowsCancelButton(false, animated: true)
        
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

