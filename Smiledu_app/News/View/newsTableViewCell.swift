//
//  newsTableViewCell.swift
//  Smiledu_app
//
//  Created by Giovanni Camana on 2/17/18.
//  Copyright © 2018 Anthony Montes Larios. All rights reserved.
//

import UIKit

class newsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newsListTitle: UILabel!
    @IBOutlet weak var newsListDate: UILabel!
    @IBOutlet weak var newsListDesc: UILabel!
    @IBOutlet weak var newsListImage: UIImageView!
    
    @IBOutlet weak var newsListImageFilter: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

